namespace coloredtorches.ModSystem
{
    using HarmonyLib;

    using Vintagestory.API.Common;
    using Vintagestory.API.Server;
    using Vintagestory.API.Client;
    using coloredtorches.Block;

    public class ColoredTorchSystem : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return true;
        }

        public override void Start(ICoreAPI api)
        {
            // Register custom torch block because some things in BlockTorch things are hardcoded and do not work with variants
            api.RegisterBlockClass("BlockColoredTorch", typeof(BlockColoredTorch));

            base.Start(api);
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            // Apply patches with harmony
            var harmony = new Harmony(this.Mod.Info.ModID);
            harmony.PatchAll();
        }

        public override void StartClientSide(ICoreClientAPI api)
        {


        }
    }
}
