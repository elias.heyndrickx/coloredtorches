namespace coloredtorches.Patch
{
    using HarmonyLib;
    using System.Drawing;
    using Vintagestory.API.Common;
    using Vintagestory.GameContent;

    [HarmonyPatch(typeof(BlockTorchHolder), "OnBlockInteractStart")]
    class TorchholderInteractStart
    {
        static bool Prefix(IWorldAccessor world, IPlayer byPlayer, BlockSelection blockSel, ref bool __result, ref BlockTorchHolder __instance)
        {
            if (__instance.Empty)
            {
                ItemStack heldStack = byPlayer.InventoryManager.ActiveHotbarSlot.Itemstack;

                if(heldStack != null)
                {
                    string path = heldStack.Collectible.Code.Path;
                    if (path.Equals("torch-up"))
                    {
                        PlaceTorch(world, byPlayer, blockSel, null, ref __instance);
                        __result = true;
                        return false;
                    }
                    if(path.StartsWith("coloredtorch-") && path.EndsWith("-up"))
                    {
                        string color = path.Split('-')[1];
                        PlaceTorch(world, byPlayer, blockSel, color, ref __instance);
                        __result = true;
                        return false;
                    }
                }
            }
            else
            {
                ItemStack stack = new ItemStack(world.GetBlock(new AssetLocation("torch-up")));

                // Check if we are dealing with a colored torch
                string colorVariant = ColoredVariant(ref __instance);
                if (colorVariant != null)
                    stack = new ItemStack(world.GetBlock(new AssetLocation($"coloredtorches:coloredtorch-{colorVariant}-lit-up")));

                if (byPlayer.InventoryManager.TryGiveItemstack(stack, true))
                {
                    Block filledBlock = world.GetBlock(__instance.CodeWithVariant("state", "empty"));
                    world.BlockAccessor.SetBlock(filledBlock.BlockId, blockSel.Position);

                    if (__instance.Sounds?.Place != null)
                    {
                        world.PlaySoundAt(__instance.Sounds.Place, blockSel.Position.X, blockSel.Position.Y, blockSel.Position.Z, byPlayer);
                    }

                    __result = true;
                    return false;
                }
            }

            // Run the original method so it will call the base class method instead
            return true;
        }

        static private void PlaceTorch(IWorldAccessor world, IPlayer byPlayer, BlockSelection blockSel, string color, ref BlockTorchHolder __instance)
        {
            byPlayer.InventoryManager.ActiveHotbarSlot.TakeOut(1);
            byPlayer.InventoryManager.ActiveHotbarSlot.MarkDirty();

            string filledState = "filled";
            if (color != null)
                filledState += color;

            Block filledBlock = world.GetBlock(__instance.CodeWithVariant("state", filledState));
            world.BlockAccessor.SetBlock(filledBlock.BlockId, blockSel.Position);

            if (__instance.Sounds?.Place != null)
            {
                world.PlaySoundAt(__instance.Sounds.Place, blockSel.Position.X, blockSel.Position.Y, blockSel.Position.Z, byPlayer);
            }
        }

        static private string ColoredVariant(ref BlockTorchHolder __instance)
        {
            if (__instance.Variant["state"].StartsWith("filled"))
            {
                string remainder = __instance.Variant["state"].Replace("filled", "");

                // Attempt to create a new color with the color name
                Color c = Color.FromName(remainder.ToLower());

                // Check if the colorname is valid
                if (c.R + c.G + c.B + c.A != 0)
                    return remainder;
            }

            return null;
        }
    }
}
