namespace coloredtorches.Patch
{
    using HarmonyLib;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.GameContent;

    [HarmonyPatch(typeof(BlockTorchHolder), "GetPlacedBlockInteractionHelp")]
    class TorchHolderInteractionHelp
    {
        static bool Prefix(IWorldAccessor world, BlockSelection selection, IPlayer forPlayer, ref WorldInteraction[] __result, ref BlockTorchHolder __instance)
        {
            // Appending the default method just adds the same message twice...
            // WorldInteraction[] help = BaseTorchHolderInteractionHelp.InteractionHelp(__instance, world, selection, forPlayer);

            if (__instance.Empty)
            {
                ItemStack[] stacks = new ItemStack[]
                {
                    new ItemStack(world.GetBlock(new AssetLocation("torch-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-red-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-orange-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-yellow-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-green-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-cyan-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-blue-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-purple-lit-up"))),
                    new ItemStack(world.GetBlock(new AssetLocation("coloredtorches:coloredtorch-pink-lit-up")))
                };

                __result = new WorldInteraction[]
                {
                    new WorldInteraction()
                    {
                        ActionLangCode = "blockhelp-torchholder-addtorch",
                        MouseButton = EnumMouseButton.Right,
                        Itemstacks = stacks
                    }
                };

                return false;
            }
            else
            {
                __result = new WorldInteraction[]
                {
                    new WorldInteraction()
                    {
                        ActionLangCode = "blockhelp-torchholder-removetorch",
                        MouseButton = EnumMouseButton.Right,
                        Itemstacks = null
                    }
                };

                return false;
            }
        }

    }
}
