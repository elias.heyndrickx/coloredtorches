namespace coloredtorches.Patch
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using HarmonyLib;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.GameContent;

    [HarmonyPatch]
    class BaseTorchHolderInteractionHelp
    {
        [HarmonyReversePatch]
        [HarmonyPatch(typeof(BlockTorchHolder), "GetPlacedBlockInteractionHelp")]
        public static WorldInteraction[] InteractionHelp(object instance, IWorldAccessor world, BlockSelection selection, IPlayer forPlayer)
        {
            // its a stub so it has no initial content
            throw new NotImplementedException("It's a stub");
        }
    }
}
